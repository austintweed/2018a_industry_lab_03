package ictgradschool.industry.lab03.ex05;

/**
 * Created by anhyd on 3/03/2017.
 */
public class ExerciseFive {

    /**
     * Runs an example program.
     */
    private void start() {
        String letters = "X X O O X O X O X ";

        String row1 = getRow(letters, 1);

        String row2 = getRow(letters, 2);

        String row3 = getRow(letters, 3);

        printRows(row1, row2, row3);

        String leftDiagonal = getLeftDiagonal(row1, row2, row3);

        printDiagonal(leftDiagonal);
    }

    /**
     * TODO Implement this
     */
    public String getRow(String letters, int row) {

        return null;
    }

    /**
     * TODO Implement this
     */
    public void printRows(String row1, String row2, String row3) {

    }

    /**
     * TODO Implement this
     */
    public String getLeftDiagonal(String row1, String row2, String row3) {

        return null;
    }

    /**
     * TODO Implement this
     */
    public void printDiagonal(String leftDiagonal) {

    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        ExerciseFive ex = new ExerciseFive();
        ex.start();
    }
}
/*Exercise Five: Noughts and crosses
Write a program to extract 3 rows of 6 characters from a String of 18 characters. The
program should print out the 3 rows, followed by the left diagonal of the 3 rows. The String
method substring() can be used to extract the required characters.
The program is to be written so that each task is in a separate method. You need to write 4
methods, one method for each of the following tasks:
● Extracting and returning a specified substring of 6 characters from the String
● Printing the 3 rows
● Extracting and returning the left diagonal
● Printing the diagonal
Here is an example of the output of the program:
X X O
O X O
X O X
Diagonal: X X X
The methods will be used as follows for the program:
String letters = " X X O O X O X O X ";
String row1 = getRow ( letters , 1 );
String row2 = getRow ( letters , 2 );
String row3 = getRow ( letters , 3 );
printRows ( row1 , row2 , row3 );
String leftDiagonal = getLeftDiagonal ( row1 , row2 , row3 );
printDiagonal ( leftDiagonal );
You may also run the provided test,
ictgradschool.industry.lab03.ex05.TestExerciseFive , to test the getRow(...) and
getLeftDiagonal(…) methods.*/
