package ictgradschool.industry.lab03.ex02;

import ictgradschool.Keyboard;
import jdk.nashorn.internal.ir.IfNode;
import sun.text.resources.cldr.ia.FormatData_ia;

import java.awt.*;
import java.awt.print.Printable;
import java.time.chrono.MinguoChronology;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class ExerciseTwo {

    /**
     * TODO Your code here. You may also write additional methods if you like.
     */
    private void start() {

    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();

        System.out.println("Lower bound? ");
        String EnterNumberFirst = Keyboard.readInput();
        System.out.println("Upper bound? ");
        String EnterNumberSecond = Keyboard.readInput();
        int First = Integer.parseInt(EnterNumberFirst);
        int Second = Integer.parseInt(EnterNumberSecond);

        int FirstNumberPlacement = Math.min(First, Second);
        int SecondNumberPlacement = Math.max(First, Second);

        int randomizer = (int) Math.round(FirstNumberPlacement + (SecondNumberPlacement + 1 - FirstNumberPlacement) * Math.random());

        int randomone = randomizer;
        int randomtwo = randomizer;
        int randomthree = randomizer;


        System.out.println("3 randomly generated numbers: " + randomone + randomtwo + randomthree);


        int a = Math.min(randomone, randomtwo);
        int b = Math.min(randomtwo, randomthree);


        System.out.println("Smallest number is " + Math.min(a, b));
    }
}

/*

public class ExerciseOne {
    public static void main(String[] args) {
        int a = 7;
        int b = 1;
        int c = a + 2;
        a = b;
        b = c;
        c = c + 1;

       a becomes 1, b becomes 3, c becomes 4.

ExerciseTwo

"The fruit is red apple"
"The fruit is orange"
"The fruit is red apple"
"The fruit is red apple"
"The fruit is navel orange"
"The fruit is green apple"

*/
