package ictgradschool.industry.lab03.ex04;

import com.sun.image.codec.jpeg.TruncatedFileException;
import ictgradschool.Keyboard;

import java.util.Base64;

/**
 * Write a program that prompts the user to enter an amount and a number of decimal places.  The program should then
 * truncate the amount to the user-specified number of decimal places using String methods.
 * <p>
 * <p>To truncate the amount to the user-specified number of decimal places, the String method indexOf() should be used
 * to find the position of the decimal point, and the method substring() should then be used to extract the amount to
 * the user-specified number of decimal places.  The program is to be written so that each task is in a separate method.
 * You need to write four methods, one method for each of the following tasks:</p>
 * <ul>
 * <li>Printing the prompt and reading the amount from the user</li>
 * <li>Printing the prompt and reading the number of decimal places from the user</li>
 * <li>Truncating the amount to the user-specified number of DP's</li>
 * <li>Printing the truncated amount</li>
 * </ul>
 */
public class ExerciseFour {

    private void start() {

        // TODO Use other methods you create to implement this program's functionality.
        String number = getTruncatedAmount();

        int decimalPlaces = getDecimalPlaces();

        fullyTruncated(number, decimalPlaces);

        String printTruncatedNumber;
    }






    // TODO Write a method which prompts the user and reads the amount to truncate from the Keyboard

    private String getTruncatedAmount() {

        System.out.println("Please enter an amount: ");
        String truncatedAmount = Keyboard.readInput();

        return truncatedAmount;

    }


    // TODO Write a method which prompts the user and reads the number of DP's from the Keyboard

    private int getDecimalPlaces() {

        System.out.println("Please enter the number of decimal places: ");
        int decimalPlaces = Integer.parseInt(Keyboard.readInput());


        return decimalPlaces;
    }

    // TODO Write a method which truncates the specified number to the specified number of DP's


    private String fullyTruncated(String number, int decimalPlaces) {


        //String truncatedAmount = Keyboard.readInput();
        //String decimalPlaces = (Keyboard.readInput());
        int decimalPlaceIndex = number.indexOf('.');

        String j = number.substring(0, decimalPlaceIndex + decimalPlaces);
        return j;





        /*
         * <p>To truncate the amount to the user-specified number of decimal places, the String method indexOf() should be used
                * to find the position of the decimal point, and the method substring() should then be used to extract the amount to
                * the user-specified number of decimal places.  The program is to be written so that each task is in a separate method.
                * You need to write four methods, one method for each of the following tasks:</p>*/

    }

    // TODO Write a method which prints the truncated amount

    public void printTruncatedNumber(String number, String truncatedAmount) {
        System.out.print("Amount truncated to " + truncatedAmount);
        System.out.println("decimal places is: " + number);
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        ExerciseFour ex = new ExerciseFour();
        ex.start();
    }
}


/*

Please enter an amount: 45.893
Please enter the number of decimal places: 1
Amount truncated to 1 decimal places is: 45.8
To truncate the amount to the user-specified number of decimal places, the String method
indexOf() should be used to find the position of the decimal point, and the method
substring() should then be used to extract the amount to the user-specified number of
decimal places. The program is to be written so that each task is in a separate method. You
need to write four methods, one method for each of the following tasks:
● Printing the prompt and reading the amount from the user
● Printing the prompt and reading the number of decimal places from the user
● Truncating the amount to the user-specified number of decimal places
● Printing the truncated amount
 */
